[M4x10 screws]:Parts.yaml#M4x10PanSteel
# Test Page 2

{{BOM}}

## Put screws into the widget {pagestep}

* Get a [3mm Allen key]{Qty: 1, Cat: tool} ready
* Take three [M4x10 screws]{Qty: 3} and screw them into the [widget]{Qty: 1}

## Put more screws into the widget {pagestep}

* Find the [3mm Allen key]{Qty: 1, Cat: tool} again
* Take two more [M4x10 screws]{Qty: 2} and screw them into the same widget

