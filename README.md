# Syntax cheatsheet

* https://gitbuilding.io/usage/buildup/

Enlazar a otra pagina:

* `Print the components` es el texto que se muestra. Puede reemplazarse por un `.` para usar el titulo de la pagina (H1) en su `.md`.
* `CO2_sensor_main.md` es el nombre del markdown con el contenido de la pagina.
* `step` no sabemos jaja

```
[Print the components](CO2_sensor_main.md){step}
```

# Gitlab Pages and YAML help

* https://getpublii.com/docs/host-static-website-gitlab-pages.html
* https://stackoverflow.com/questions/31313452/yaml-mapping-values-are-not-allowed-in-this-context/31335106
* http://www.yamllint.com/

# Gitbuilding issues

Issues para reportar:

* ?

# Gitbuilding HTML customization

* general: https://gitbuilding.io/usage/buildconfsyntax/#customising-html-output
* head tag: https://gitlab.com/gitbuilding/gitbuilding/-/blob/master/gitbuilding/templates/full_page.html.jinja
