# Sensores CO2

Instrucciones de ensamblado del sensor de CO2.

Encontra pasos detallados para cada variante en estas páginas:

* [.](co2_variante1.md){step}
* [.](co2_variante2.md){step}

La lista completa de materiales y componentes para construir todas estas variantes se encuentra en [esta página]{BOM}.
