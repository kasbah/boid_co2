# Sensor CO2 - Variante 1

Sensor de CO2 basado en MQ135 y el microcontrolador ESP8266 (marcado como ESP12E).

Empezamos por este ya que el sensor de CO2 tipo NDIR no llegó todavía.

{{BOM}}


## Probar el microcontrolador {pagestep}

Encontrar el microcontrolador [nodemcu]{qty: 1} y el [cable micro USB]{qty: 1}.

Conectar el microcontrolador a la computadora, y abrir el Arduino IDE.

Configurar el Arduino IDE para usar ESP8226 siguiendo las instrucciones: https://github.com/esp8266/Arduino#installing-with-boards-manager

Subir el siguiente sketch, y verificar en el serial monitor que aparezca el mensaje `"Hola Boid!"` (asegurar que el baudrate este a 115200).

```
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Hola Boid!");
}

void loop() {
  // put your main code here, to run repeatedly:

}
```

## Probar el sensor MQ135 con LCD display {pagestep}

Usamos de base esta guia: https://how2electronics.com/iot-air-quality-index-monitoring-esp8266/


### Preprar library MQ135

Instalar la library `MQ135` en el Arduino IDE

Descargar: https://github.com/GeorgK/MQ135/archive/refs/heads/master.zip

Agregar la library al Arduino IDE usando el menú `Sketch -> Include Library -> Add .ZIP Library...` y seleccionar el archivo bajado.

La library se incluye en el sketch con la linea `#include "MQ135.h"` al inicio del programa.


### Preprar library LCD display

- [ ] Pendiente.

### Subir sketch

Una vez instalada la library necesaria, y con el ESP8226 conectado correctamente, subir el siguiente sketch de prueba:

```Cpp
#include "MQ135.h"

void setup(){
  Serial.begin(115200);
}


void loop(){
    MQ135 gasSensor = MQ135(A0);
    float air_quality = gasSensor.getPPM();
    Serial.print("Air Quality: ");
    Serial.print(air_quality);
    Serial.println("  PPM");
    Serial.println();

    Serial.println("Waiting...");

    delay(2000);
}
```

Abrir el _serial monitor_ (a baud rate 115200) y esperar a ver los mensajes del programa.

Todavía no conectamos el sensor, pero si hay mensajes, está bien.

Finalmente, desconectar el cable USB de la PC.

### Conexiones LCD display

Haremos las conexiones usando un [mini protoboard]{Qty:1}. Si usan un protoboard normal, pueden montar el microcontrolador dejando pines a cada costado, haciendo fuerza hasta que entre del todo (puede ser que cueste, apretar fuerte sobre los pines para que entre). No hacemos esto porque el mini protoboard solo nos deja una hilera a cada lado, y es insuficiente (porque I2C y Vin estan en lados opuestos).

Usaremos un [display]{qty:1} LCD, controlado con el protocolo I2C.

- [ ] TO-DO
- [ ] Agregar diagrama fritzing o equivalente.

### Conexiones MQ135

Luego conectar el sensor [MQ135]{qty:1} al protoboard usando tres [cables jumper HM]{qty:3}.

Finalmente el protoboard al microcontrolador con otros tres [cables jumper HM]{qty:3}.

Las conexiones deberian respetar la siguiente tabla:

| Pin ESP8226 | Pin MQ135 |
|-----|-----|
| Vin  | VCC |
| GND | GND |
| A0  | A0  |

- [ ] Agregar diagrama fritzing o equivalente.

Este sensor usa más corriente de la que puede proporcionar el puerto USB de una PC, así que habrá que usar una [fuente USB]{qty:1} de pared.

Conectar el cable usb a la fuente (y la fuente a la toma), y esperar unos segundos hasta ver los mensajes correctos en el display LCD.

## Probar el WiFi {pagestep}

- [ ] TO-DO

## Probar el IoT {pagestep}

